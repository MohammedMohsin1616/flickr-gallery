//
//  AlamofireRequest.swift
//  InmoblyTask
//
//  Created by Mohammed Mohsin Sayed on 4/25/19.
//  Copyright © 2019 Mohammed Mohsin Sayed. All rights reserved.
//

import UIKit
import Alamofire

class Service {
    
    class func request<T: Codable>(url: String, dateFormate: String?, method: HTTPMethod, parameters: Parameters?, headers: HTTPHeaders?,
                                   callBack:@escaping (T) -> Void,
                                   failureHandler:@escaping (Error) -> Void) {
        
        let jsonDecodeer = JSONDecoder()
        
        if let _ = dateFormate {
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = dateFormate
            jsonDecodeer.dateDecodingStrategy = .formatted(dateFormatter)
        }
        
        Alamofire.request(url, method: method, parameters: parameters, headers: headers).validate().responseJSON { (response) in
            
            switch response.result {
            case .success:
                
                if let _ = response.result.value {
                    
                    guard let resultData = response.data else { fatalError() }
                    
                    do {
                        let basicResponse = try jsonDecodeer.decode(T.self, from: resultData)
                        callBack(basicResponse)
                        
                    }
                    catch(let error) {
                        
                        failureHandler(error)
                    }
                }
                
            case .failure(let error):
                failureHandler(error)
            }
        }
        
    }
    
}
