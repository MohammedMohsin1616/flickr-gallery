//
//  Photo.swift
//  InmoblyTask
//
//  Created by Mohammed Mohsin Sayed on 4/25/19.
//  Copyright © 2019 Mohammed Mohsin Sayed. All rights reserved.
//

import Foundation

struct FlickPhoto: Codable {
    
    var url_m: String?
    var owner: String?
}

struct FlickerObject: Codable {
    
    var photo: [FlickPhoto]?
    var page: Int?
    var total: Int?
    var perpage: Int?
    var pages: Int?
}

struct FlickrResponse: Codable {
  
    var photos: FlickerObject?
    var stat: String?
}

