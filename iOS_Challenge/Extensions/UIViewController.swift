//
//  UIViewController.swift
//  iOS_Challenge
//
//  Created by Mohammed Mohsin Sayed on 4/25/19.
//  Copyright © 2019 Mohammed Mohsin Sayed. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

extension GalleryViewController: NVActivityIndicatorViewable {
    
    func startLoading()  {
        let size = CGSize(width: 50, height: 50)
        NVActivityIndicatorView.DEFAULT_COLOR = #colorLiteral(red: 1, green: 0.4196078431, blue: 0.5058823529, alpha: 0.75) 
        NVActivityIndicatorView.DEFAULT_BLOCKER_BACKGROUND_COLOR =  UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        self.startAnimating(size, message: "", type: NVActivityIndicatorType.orbit)
    }
    
    func stopLoading()  {
        self.stopAnimating()
    }
    
    func responseToOffline() {
        
        let viewController = UIStoryboard.init(name: "Gallery", bundle: nil).instantiateViewController(withIdentifier: "DisconnectedVC") as! DisconnectedVC
        
        self.disconnectedObj = viewController
        self.disconnectedObj?.delegate = self
        
        viewController.modalPresentationStyle = .overCurrentContext
        viewController.modalTransitionStyle = .crossDissolve
        self.present(viewController, animated: true, completion: nil)
        
    }
}
