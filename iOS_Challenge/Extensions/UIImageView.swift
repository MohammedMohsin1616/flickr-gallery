//
//  UIImageView.swift
//  InmoblyTask
//
//  Created by Mohammed Mohsin Sayed on 4/25/19.
//  Copyright © 2019 Mohammed Mohsin Sayed. All rights reserved.
//

import UIKit

let imageCache = NSCache<NSString, UIImage>()

class CustomImageView: UIImageView {
    
    func downloadImage(from url: String, contentMode mode: UIView.ContentMode) {
        
        let imageUrlString = url
        
        if let imageURL = URL(string: url) {
            
            self.image = nil
            
            if let imageForCache = imageCache.object(forKey: NSString(string: url)) {
                
                self.image = imageForCache
                return
            }
            
            URLSession.shared.dataTask(with: imageURL) { data, response, error in
                guard
                    let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                    let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                    let data = data, error == nil,
                    let image = UIImage(data: data)
                    else { return }
                DispatchQueue.main.async() {
                    
                    let imageToCache = image
                    
                    if imageUrlString == url {
                        
                        self.image = imageToCache
                    }
                    
                    imageCache.setObject(imageToCache, forKey: NSString(string: url))
                    self.contentMode = mode
                }
                }.resume()
        }
        
        
    }
    
    
}

class CirculaImageView: UIImageView {
    
    
}

