//
//  Photo+CoreDataProperties.swift
//  iOS_Challenge
//
//  Created by Mohammed Mohsin Sayed on 4/25/19.
//  Copyright © 2019 Mohammed Mohsin Sayed. All rights reserved.
//
//

import Foundation
import CoreData


extension Photo {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Photo> {
        return NSFetchRequest<Photo>(entityName: "Photo")
    }

    @NSManaged public var url_m: String?
    @NSManaged public var owner_name: String?
    @NSManaged public var imageData: Data? 

}
