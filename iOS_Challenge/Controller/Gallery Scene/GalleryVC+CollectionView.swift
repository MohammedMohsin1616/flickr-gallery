//
//  GalleryVC+CollectionViewDelegate.swift
//  iOS_Challenge
//
//  Created by Mohammed Mohsin Sayed on 4/26/19.
//  Copyright © 2019 Mohammed Mohsin Sayed. All rights reserved.
//

import UIKit

extension GalleryViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if isOffline {
            
            return self.savedPhotos.count
        }
        return self.photos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCell", for: indexPath) as! PhotoCell
        
        DispatchQueue.main.async {
            
            if !self.isOffline {
                let photo = self.photos[indexPath.item]
                cell.setupCell(photo: photo)
            }
            else {
                let photo = self.savedPhotos[indexPath.item]
                cell.setupSavedPhoto(photo: photo) 
            }
        }
        
        return cell
    }
}

extension GalleryViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        imageIndex = indexPath.item
        
        let cell = collectionView.cellForItem(at: indexPath) as! PhotoCell
        if let image = cell.photoImage.image {
 
            createScrollView() 
            setupFullScreenImage(image)
        }
        
        
    }
   
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if !isOffline {
            
            if let photoCell = cell as? PhotoCell {
                
                let photo = photos[indexPath.item]
                if let image = photoCell.photoImage.image {
                    
                    ref.storeImage(image, photo)
                }
                
            }
        }
    }
    
    func setupFullScreenImage(_ image: UIImage) {
        
        fullScreenImage.image = image
        self.fullScreenImage.frame = self.view.viewWithTag(100)!.bounds
        self.fullScreenImage.backgroundColor = .black
        self.fullScreenImage.contentMode = .scaleAspectFit
        self.fullScreenImage.isUserInteractionEnabled = true
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.dismissFullscreenImage))
        swipe.direction = .down
        self.fullScreenImage.addGestureRecognizer(swipe)
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(swiped))
        swipeRight.direction = UISwipeGestureRecognizer.Direction.right
        self.fullScreenImage.addGestureRecognizer(swipeRight)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(swiped))
        swipeLeft.direction = UISwipeGestureRecognizer.Direction.left
        self.fullScreenImage.addGestureRecognizer(swipeLeft)
    }
    
    func createScrollView() {
                
        let scrollview = UIScrollView.init(frame: UIScreen.main.bounds)
        scrollview.minimumZoomScale = 1.0
        scrollview.maximumZoomScale = 10.0
        scrollview.zoomScale = 1.0
        scrollview.delegate = self
        scrollview.tag = 100
        self.view.addSubview(scrollview)
        scrollview.addSubview(fullScreenImage)

    }
    
    @objc func swiped(gesture: UIGestureRecognizer) {
        
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            
            switch swipeGesture.direction {
                
            case .left:
                
                if imageIndex < photos.count - 1 || imageIndex < savedPhotos.count - 1 {
                    imageIndex = imageIndex + 1
                }
                
            case .right:
                
                if imageIndex > 0 {
                    imageIndex = imageIndex - 1
                }
                
            default:
                break
            }
             getNextImage()
            
        }
    }
    
    func getNextImage() {
        
        if !isOffline {
            
            if let cell = photosCollectionView.cellForItem(at: IndexPath(item: imageIndex, section: 0)) as?
                PhotoCell {
                
                if let image = cell.photoImage.image {
                    
                    fullScreenImage.image = image
                }
                else {
                    reDownload()
                }
            }
            else {
                
                reDownload()
            }
        }
        
        else {
            
            let photos = self.ref.getSavedPhotos()
            let nextImage = UIImage(data: photos[imageIndex].imageData!)
            fullScreenImage.image = nextImage! 
        }
    }
    
    func reDownload() {
        
        if photos.count > 0 {
            let photo = photos[imageIndex]
            fullScreenImage.downloadImage(from: photo.url_m!, contentMode: .scaleAspectFit)
        }

    }
}
