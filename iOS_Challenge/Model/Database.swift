//
//  Database.swift
//  iOS_Challenge
//
//  Created by Mohammed Mohsin Sayed on 4/26/19.
//  Copyright © 2019 Mohammed Mohsin Sayed. All rights reserved.
//

import Foundation
import CoreData

class Database {
    
//    func storePhotos(allPhotos: [FlickPhoto]) {
//
//        for photo in allPhotos {
//
//            if let url = photo.url_m,
//                let owner = photo.owner {
//
//                let newPhoto = Photo(context: PersistanceService.context)
//                newPhoto.owner_name = owner
//                newPhoto.url_m = url
//
//                let image = UIImage(named: "flickr")
//                let data = image?.jpegData(compressionQuality: 1)
//                newPhoto.imageData = data
//            }
//
//        }
//
//        PersistanceService.saveContext()
//    }
    var storedURLs = [String]()
    
    func storeImage(_ image: UIImage, _ photo: FlickPhoto) {
        
        if let url = photo.url_m,
            let owner = photo.owner {
            
            if let _ = storedURLs.index(of: url) {
                
            }
            else {
                
                let newPhoto = Photo(context: PersistanceService.context)
                newPhoto.owner_name = owner
                newPhoto.url_m = url
                storedURLs.append(url)
                
                let data = image.jpegData(compressionQuality: 1)
                newPhoto.imageData = data
            }
        }
        
        PersistanceService.saveContext()
    }
    
    func deleteStoredPhotos() {
        
        let fetchRequest: NSFetchRequest<Photo> = Photo.fetchRequest()
        
        do {
            
            let savedPhotos = try PersistanceService.context.fetch(fetchRequest)
            for photo in savedPhotos {
                
                PersistanceService.context.delete(photo)
            }
            
            try PersistanceService.context.save()
        }
        catch (let error){
            
            print(error.localizedDescription)
        }
    }
    
    func getSavedPhotos() -> [Photo]{
        
        var savedPhotos = [Photo]()
        let fetchRequest: NSFetchRequest<Photo> = Photo.fetchRequest()
        
        do {
            
            savedPhotos = try PersistanceService.context.fetch(fetchRequest)
            
        }
        catch (let error){
            
            print(error.localizedDescription)
        }
        
        return savedPhotos
    }
}
