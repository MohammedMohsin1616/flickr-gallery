//
//  PhotoCell.swift
//  iOS_Challenge
//
//  Created by Mohammed Mohsin Sayed on 4/25/19.
//  Copyright © 2019 Mohammed Mohsin Sayed. All rights reserved.
//

import UIKit

class PhotoCell: UICollectionViewCell {
    
    @IBOutlet weak var photoImage: CustomImageView!
    @IBOutlet weak var ownerName: UILabel!
    let ref = Database()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func setupCell(photo: FlickPhoto) {
        
        if let url = photo.url_m,
            let owner_name = photo.owner {
            
            self.photoImage.downloadImage(from: url, contentMode: .scaleAspectFill)
            ownerName.text = owner_name
            
        }
        
        
    }
    func setupSavedPhoto(photo: Photo) {
        
        if let owner_name = photo.owner_name {
            
            guard let image = UIImage(data: photo.imageData!) else {
                return
            }
            
            self.photoImage.image = image
            ownerName.text = owner_name
        }
    }
}
