//
//  DisconnectedVC.swift
//  iOS_Challenge
//
//  Created by Mohammed Mohsin Sayed on 4/26/19.
//  Copyright © 2019 Mohammed Mohsin Sayed. All rights reserved.
//

import UIKit

protocol ReconnectingDelegate: class {
    func didTapOnReconnect()
}

class DisconnectedVC: UIViewController {

    weak var delegate: ReconnectingDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = UIColor(white: 0, alpha: 0.7)
    }
    
    
    @IBAction func reConnectButtonTapped() {
        
        delegate?.didTapOnReconnect()
    }

}
