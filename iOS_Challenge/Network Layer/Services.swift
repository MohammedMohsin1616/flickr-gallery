//
//  Services.swift
//  InmoblyTask
//
//  Created by Mohammed Mohsin Sayed on 4/25/19.
//  Copyright © 2019 Mohammed Mohsin Sayed. All rights reserved.
//

import UIKit
import Alamofire

class Services {
    
    class func getRecentPhotos(page: Int, callback: @escaping (FlickrResponse) -> Void, failureHandler: @escaping (Error) -> Void) {
        
        let basicURL = URLs.basicURL
        let method = URLs.getRecentURL
        let api_key = Constants.API_KEY
        let extras = Constants.extras
        let format = "json"
        let nojsoncallback = "1"
        
        let parameters = ["api_key": api_key, "method": method, "extras": extras
            , "format": format, "nojsoncallback": nojsoncallback, "page": page] as [String : Any]
        
        Service.request(url: basicURL, dateFormate: nil, method: HTTPMethod.get, parameters: parameters, headers: nil, callBack: { (response: FlickrResponse) in
            
            callback(response)
            
        }) { (error) in
            failureHandler(error)
        }
    }
}
