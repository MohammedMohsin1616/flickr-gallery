//
//  ViewController.swift
//  iOS_Challenge
//
//  Created by Mohammed Mohsin Sayed on 4/25/19.
//  Copyright © 2019 Mohammed Mohsin Sayed. All rights reserved.
//

import UIKit
import CoreData

class GalleryViewController: UIViewController {
    
    @IBOutlet weak var imageView: CustomImageView!
    @IBOutlet weak var photosCollectionView: UICollectionView!
    
    var photos = [FlickPhoto]()
    var savedPhotos = [Photo]()
    var page = 1
    var total_pages = 0
    var refreshControl = UIRefreshControl() // pagination
    var firstTime = true
    var refresher = UIRefreshControl() // pull to refresh
    var isRefreshing = false
    var disconnectedObj: DisconnectedVC?
    var isOffline = true
    let ref = Database()
    var fullScreenImage = CustomImageView()
    var imageIndex = 0 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        setupCollectionView()
        setupRefreshControl()
        startApp()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if !isOffline {
            
            photosCollectionView.addSubview(refresher)
            photosCollectionView.bottomRefreshControl = refreshControl
        }
    }
    
    
    private func setupCollectionView() {
        
        photosCollectionView.delegate = self
        photosCollectionView.dataSource = self
        photosCollectionView.alwaysBounceVertical = true
        
    }
    
    private func setupRefreshControl() {
        
        refreshControl = UIRefreshControl.init(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        refreshControl.triggerVerticalOffset = 50.0
        refreshControl.addTarget(self, action: #selector(paginateMore), for: .valueChanged)
        setupPullToRefreshController()
    }
    
    private func setupPullToRefreshController() {
        
        refresher.attributedTitle = NSAttributedString(string: "")
        refresher.addTarget(self, action: #selector(refresh), for: .valueChanged)
        
    }
    
    @objc func refresh() {
        
        self.isRefreshing = true
        if Helper.isConnectedToNetwork() {
            fetchPhotos(isRefreshing: self.isRefreshing)
        }
        else {
            responseToOffline()
        }
    }
    
    @objc func paginateMore() {
        
        if firstTime || canPaginate(){
            
            self.isRefreshing = false 
            if Helper.isConnectedToNetwork() {
                fetchPhotos(isRefreshing: self.isRefreshing)
            }
            else {
                responseToOffline()
            }
            
        }
        else {
            self.refreshControl.endRefreshing()
        }
        
    }
    
    private func startApp() {
        
        if Helper.isConnectedToNetwork() {
            
            self.isOffline = false 
            fetchPhotos(isRefreshing: self.isRefreshing)
        }
        else {
            getSavedPhotos()
        }
    }
    
    private func fetchPhotos(isRefreshing: Bool) {
        
        startLoading()
        Services.getRecentPhotos(page: self.page, callback: { (result) in
            
            self.stopLoading()
            if let flickObj = result.photos {
                
                if let totalPhotos = flickObj.photo {
                    
                    if totalPhotos.count > 0 {
                        
                        DispatchQueue.main.async {
                            
                            if !isRefreshing {
                                self.photos = self.photos + flickObj.photo!
                                
                                if self.firstTime {
                                    self.ref.deleteStoredPhotos()
                                }
                            }
                            else {
                                self.photos = flickObj.photo!
                                self.deleteSavedPhotos()
                                self.refresher.endRefreshing()
                            }
                            
                            self.reloadData()
                            self.refreshControl.endRefreshing()
                            self.total_pages = flickObj.pages!
                            self.page = self.page + 1
                            self.firstTime = false
                            //self.saveItToDB()
                        }
                    }
                }
            }
        }) { (error) in
            
            print(error.localizedDescription)
        }
   
    }
    
     func saveItToDB() {
      
        
    }
    
    private func canPaginate() -> Bool {
    
        if self.page < total_pages {
            
            return true
        }
        return false
    }
    
    @objc func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {

        UIView.animate(withDuration: 0.5, animations: {
            sender.view?.frame = CGRect(x: self.view.frame.maxX/2, y: self.view.frame.maxY, width: 0, height: 0)
        }, completion: { _ in
            
            let scrollview = self.view.viewWithTag(100)
            scrollview?.removeFromSuperview() 
            sender.view?.removeFromSuperview()
            
        })
    }
    
    func reconnecting() {
        
        self.dismiss(animated: true, completion: nil)
        if isRefreshing {
            refresh()
        }
        else {
            paginateMore()
        }
    }
    
     func getSavedPhotos() {
        
       self.savedPhotos = ref.getSavedPhotos()
    }
    
    private func deleteSavedPhotos() {
        
        ref.deleteStoredPhotos()
    }
    
    private func reloadData() {

        photosCollectionView.reloadData()
        photosCollectionView.layoutIfNeeded()
        
    }

}
