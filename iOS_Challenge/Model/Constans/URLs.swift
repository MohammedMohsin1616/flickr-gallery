//
//  URLs.swift
//  iOS_Challenge
//
//  Created by Mohammed Mohsin Sayed on 4/25/19.
//  Copyright © 2019 Mohammed Mohsin Sayed. All rights reserved.
//

import Foundation

class URLs {
    
    static let basicURL = "https://api.flickr.com/services/rest/?method="
    
    static let getRecentURL = "flickr.photos.getRecent"
    static let gerPopularURL = "flickr.photos.getPopular"
}
