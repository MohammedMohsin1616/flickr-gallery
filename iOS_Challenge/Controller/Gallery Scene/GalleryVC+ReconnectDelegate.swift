//
//  GalleryVC+ReconnectDelegate.swift
//  iOS_Challenge
//
//  Created by Mohammed Mohsin Sayed on 4/26/19.
//  Copyright © 2019 Mohammed Mohsin Sayed. All rights reserved.
//

import UIKit

extension GalleryViewController: ReconnectingDelegate {
    
    func didTapOnReconnect() {
       
        self.reconnecting()  
    }
}
