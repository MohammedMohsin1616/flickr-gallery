//
//  SplashViewController.swift
//  iOS_Challenge
//
//  Created by Mohammed Mohsin Sayed on 4/26/19.
//  Copyright © 2019 Mohammed Mohsin Sayed. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.perform(#selector(self.presentWithCustomAnimation), with: nil, afterDelay: 3)
    }
    
    @objc func presentWithCustomAnimation() {
        
        let sb = UIStoryboard.init(name: "Gallery", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "GalleryVC") as! GalleryViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }

}
